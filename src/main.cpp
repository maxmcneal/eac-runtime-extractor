#include <iostream>
#include <windows.h>
#include <cstdint>
#include <vector>
#include <memory>
#include <algorithm>
#include <intrin.h>
#include <nmmintrin.h>

// Include the necessary headers for MinHook and NT functions.
#include <MinHook.h> // Assuming MinHook is available in the given path.

/**
 * @struct Section
 * @brief Represents a section of memory being monitored.
 */
struct Section {
    PVOID base; ///< Base address of the memory section.
    SIZE_T size; ///< Size of the memory section.
    uint32_t checksum; ///< Checksum for the section's data.
    std::unique_ptr<Section> next; ///< Pointer to the next section in the list.

    /**
     * @brief Constructs a new Section instance.
     * @param base Base address of the new memory section.
     * @param size Size of the new memory section.
     */
    Section(PVOID base, SIZE_T size) : base(base), size(size), checksum(0) {}
};

// Global variables for lock management and section tracking.
std::unique_ptr<Section> root_section; // Head of the linked list of sections.
volatile LONG global_lock = 0; // Lock for synchronizing access to the section list.
HANDLE watcher_thread = nullptr; // Handle to the watcher thread.

/**
 * @brief Acquires an exclusive lock to ensure thread-safe access to the section list.
 */
void acquire_lock() {
    while (InterlockedExchange(&global_lock, 1) == 1) {
        // Use a busy-wait loop to acquire the lock.
        YieldProcessor(); // Yield the processor to avoid wasting resources.
    }
}

/**
 * @brief Releases the previously acquired lock.
 */
void release_lock() {
    InterlockedExchange(&global_lock, 0); // Reset the lock to allow other threads to acquire it.
}

/**
 * @brief Adds a new section to the monitored sections list.
 * 
 * This function creates a new Section object and appends it to the global list
 * of monitored sections in a thread-safe manner.
 * 
 * @param base Base address of the new memory section to monitor.
 * @param size Size of the new memory section.
 */
void add_section(PVOID base, SIZE_T size) {
    acquire_lock(); // Ensure exclusive access to the list.
    std::unique_ptr<Section> new_section = std::make_unique<Section>(base, size);
    
    if (!root_section) {
        // If the list is empty, insert as the first element.
        root_section = std::move(new_section);
    } else {
        // Otherwise, find the end of the list and insert the new section.
        Section* current = root_section.get();
        while (current->next) {
            current = current->next.get();
        }
        current->next = std::move(new_section);
    }

    release_lock(); // Release the lock after insertion.
}

/**
 * @brief Removes a section from the monitored list based on its base address.
 * 
 * @param base Base address of the section to remove.
 */
void remove_section(PVOID base) {
    acquire_lock(); // Ensure exclusive access to the list.

    Section* prev = nullptr;
    Section* current = root_section.get();
    while (current) {
        if (current->base == base) {
            if (prev) {
                prev->next = std::move(current->next);
            } else {
                root_section = std::move(current->next);
            }
            break; // Exit after removal to avoid use-after-free.
        }
        prev = current;
        current = current->next.get();
    }

    release_lock(); // Release the lock after removal.
}

// Original function pointers.
ZwMapViewOfSection_t original_ZwMapViewOfSection = nullptr;
ZwUnmapViewOfSection_t original_ZwUnmapViewOfSection = nullptr;

/**
 * @brief Hook implementation for ZwMapViewOfSection.
 */
NTSTATUS WINAPI hooked_ZwMapViewOfSection(HANDLE SectionHandle, HANDLE ProcessHandle, PVOID* BaseAddress, ULONG_PTR ZeroBits, SIZE_T CommitSize, PLARGE_INTEGER SectionOffset, PSIZE_T ViewSize, SECTION_INHERIT InheritDisposition, ULONG AllocationType, ULONG Win32Protect) {
    NTSTATUS status = original_ZwMapViewOfSection(SectionHandle, ProcessHandle, BaseAddress, ZeroBits, CommitSize, SectionOffset, ViewSize, InheritDisposition, AllocationType, Win32Protect);
    
    if (NT_SUCCESS(status)) {
        add_section(*BaseAddress, *ViewSize);
    }

    return status;
}

/**
 * @brief Hook implementation for ZwUnmapViewOfSection.
 */
NTSTATUS WINAPI hooked_ZwUnmapViewOfSection(HANDLE ProcessHandle, PVOID BaseAddress) {
    remove_section(BaseAddress);
    return original_ZwUnmapViewOfSection(ProcessHandle, BaseAddress);
}

/**
 * @brief Initializes and sets up necessary hooks.
 */
void initialize_hooks() {
    if (MH_Initialize() != MH_OK) {
        std::cerr << "Failed to initialize MinHook.\n";
        return;
    }

    // Hook ZwMapViewOfSection.
    if (MH_CreateHook(&ZwMapViewOfSection, &hooked_ZwMapViewOfSection, reinterpret_cast<LPVOID*>(&original_ZwMapViewOfSection)) != MH_OK) {
        std::cerr << "Failed to create hook for ZwMapViewOfSection.\n";
        return;
    }

    // Hook ZwUnmapViewOfSection.
    if (MH_CreateHook(&ZwUnmapViewOfSection, &hooked_ZwUnmapViewOfSection, reinterpret_cast<LPVOID*>(&original_ZwUnmapViewOfSection)) != MH_OK) {
        std::cerr << "Failed to create hook for ZwUnmapViewOfSection.\n";
        return;
    }

    // Enable hooks.
    if (MH_EnableHook(MH_ALL_HOOKS) != MH_OK) {
        std::cerr << "Failed to enable hooks.\n";
        return;
    }
}

/**
 * @brief The watcher function that continuously checks the sections.
 */
DWORD WINAPI SectionWatcher(LPVOID param) {
    while (true) {
        // Acquire a lock to ensure thread-safe access to the global list of sections.
        acquire_lock();

        // Iterate over the linked list of sections.
        std::unique_ptr<Section>* current = &root_section;
        while ((*current) && (*current)->next) {
            Section* section = (*current).get();

            // Allocate a vector to hold the section's data. Vector is used for RAII memory management.
            std::vector<uint8_t> data(section->size);
            // Check for allocation failure.
            if (data.empty()) {
                std::cerr << "Failed to allocate memory for section data\n";
                current = &(*current)->next;
                continue;
            }

            // Attempt to read the process memory corresponding to the section. This is preferred over memcpy
            // because the memory may not always be directly accessible due to various reasons (e.g., permissions).
            if (!ReadProcessMemory(GetCurrentProcess(), section->base, data.data(), section->size, nullptr)) {
                std::cerr << "Failed to read section data\n";
                // Remove the section from the list if its memory cannot be accessed.
                remove_section(section->base);
                continue; // Proceed to the next section without releasing the lock, as remove_section manages lock state.
            }

            // Define a buffer structure for potential analysis. This is just an example structure.
            struct StreamBuffer_t {
                uint32_t Unk0, Unk1, Unk2, Unk3, ProcessId, Unk4, DataSize;
                uint8_t Data[1]; // Flexible array member; actual size is determined by DataSize.
            };

            // Perform a sanity check on the data to ensure it meets expected criteria (e.g., starts with 'MZ' signature).
            auto* buffer = reinterpret_cast<StreamBuffer_t*>(data.data());
            if (data[0] != 'M' || data[1] != 'Z' || buffer->DataSize > section->size - sizeof(StreamBuffer_t)) {
                current = &(*current)->next;
                continue; // Skip sections that don't match the criteria.
            }

            // Compute a checksum for the data to detect changes or uniqueness.
            uint32_t checksum = 0;
            for (size_t i = 0; i < buffer->DataSize; ++i) {
                checksum = _mm_crc32_u8(checksum, buffer->Data[i]);
            }

            // If the checksum has changed (or if this is the first calculation), update the section's checksum and dump the data.
            if (checksum != section->checksum) {
                section->checksum = checksum;

                // Construct a filename for the dump based on the checksum.
                char filename[MAX_PATH];
                snprintf(filename, sizeof(filename), "dumps/%x.bin", checksum);

                // Attempt to open the file for writing.
                FILE* file = fopen(filename, "wb");
                if (!file) {
                    std::cerr << "Failed to open file " << filename << "\n";
                    current = &(*current)->next;
                    continue; // Skip to the next section if the file cannot be opened.
                }

                // Write the data to the file and close it.
                fwrite(buffer->Data, 1, buffer->DataSize, file);
                fclose(file);

                // Log the success of the dump operation.
                std::cout << "Dumped to " << filename << "\n";
            }

            // Move to the next section in the list.
            current = &(*current)->next;
        }

        // Release the lock after processing all sections.
        release_lock();

        // Sleep for a short period to prevent constant spinning, reducing CPU usage.
        Sleep(100); // Adjust sleep duration as necessary for your application's needs.
    }
    // The return statement is technically not reached due to the infinite loop,
    // but it is required by the function signature.
    return 0;
}

/**
 * @brief Entry point of the application.
 */
int main() {
    // Initialize hooks and start the watcher thread if necessary.
    initialize_hooks();

    // Start the SectionWatcher thread.
    watcher_thread = CreateThread(nullptr, 0, reinterpret_cast<LPTHREAD_START_ROUTINE>(section_watcher), nullptr, 0, nullptr);
    if (watcher_thread == nullptr) {
        std::cerr << "Failed to create watcher thread." << std::endl;
        uninitialize_hooks();
        return -1;
    }

    // Wait for the watcher thread to finish (in this example, it runs indefinitely).
    WaitForSingleObject(watcher_thread, INFINITE);

    return 0;
}